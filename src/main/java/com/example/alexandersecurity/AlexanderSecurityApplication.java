package com.example.alexandersecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlexanderSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlexanderSecurityApplication.class, args);
	}

}
