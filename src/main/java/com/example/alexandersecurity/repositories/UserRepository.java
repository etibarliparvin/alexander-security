package com.example.alexandersecurity.repositories;

import com.example.alexandersecurity.domain.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select u from User u join fetch u.roles r join fetch u.authorities where u.email = :email")
    Optional<User> findUserByEmail(@Param(value = "email") String email);

    @Query(value = "select u from User u join fetch u.roles r join fetch u.authorities where u.username = :username")
    Optional<User> findUserByUsername(@Param(value = "username") String username);

    Boolean existsByUsernameOrEmail(String username, String email);
}
