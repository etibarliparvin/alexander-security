package com.example.alexandersecurity.repositories;

import com.example.alexandersecurity.domain.entities.Role;
import com.example.alexandersecurity.domain.enumerations.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findRoleByRoleType(@Param(value = "roleType") RoleType roleType);
}
