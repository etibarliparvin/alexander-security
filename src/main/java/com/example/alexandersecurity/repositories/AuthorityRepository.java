package com.example.alexandersecurity.repositories;

import com.example.alexandersecurity.domain.entities.Authority;
import com.example.alexandersecurity.domain.enumerations.AuthorityType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Authority findAuthoritiesByAuthorityType(AuthorityType authorityType);
}
