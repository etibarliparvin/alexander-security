package com.example.alexandersecurity.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @PreAuthorize("hasAuthority('READ')")
    @GetMapping("/student/read")
    public String studentRead() {
        return "Role Student Authority Read";
    }

    @PreAuthorize("hasAuthority('CREATE')")
    @GetMapping("/student/create")
    public String studentCreate() {
        return "Role Student Authority Create";
    }

    @PreAuthorize("hasAuthority('READ')")
    @GetMapping("/author/read")
    public String userRead() {
        return "Role Author Authority Read";
    }

    @PreAuthorize("hasAuthority('CREATE')")
    @GetMapping("/author/create")
    public String userCreate() {
        return "Role Author Authority Create";
    }
}
