package com.example.alexandersecurity.controllers;

import com.example.alexandersecurity.domain.request.UserLoginRequest;
import com.example.alexandersecurity.domain.request.UserRegisterRequest;
import com.example.alexandersecurity.domain.response.GeneralResponse;
import com.example.alexandersecurity.domain.response.RegisterResponse;
import com.example.alexandersecurity.service.AuthenticationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final AuthenticationService authenticationService;

    @PostMapping("/signup")
    public ResponseEntity<GeneralResponse<RegisterResponse>> signUpUser(@RequestBody @Valid UserRegisterRequest request) {
        return ResponseEntity.ok(new GeneralResponse<>(authenticationService.signUp(request)));
    }

    @PostMapping("/signin")
    public ResponseEntity<GeneralResponse<RegisterResponse>> signIn(@RequestBody UserLoginRequest request) {
        log.info("Registration dto is : {}", request);
        return ResponseEntity.ok(new GeneralResponse<>(authenticationService.signIn(request)));
    }
}
