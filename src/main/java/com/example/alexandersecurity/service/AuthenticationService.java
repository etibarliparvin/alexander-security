package com.example.alexandersecurity.service;


import com.example.alexandersecurity.domain.request.UserLoginRequest;
import com.example.alexandersecurity.domain.request.UserRegisterRequest;
import com.example.alexandersecurity.domain.response.RegisterResponse;

public interface AuthenticationService {

    RegisterResponse signUp(UserRegisterRequest request);

    RegisterResponse signIn(UserLoginRequest request);
}
