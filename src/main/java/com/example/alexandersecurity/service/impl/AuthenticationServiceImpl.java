package com.example.alexandersecurity.service.impl;

import com.example.alexandersecurity.domain.entities.Authority;
import com.example.alexandersecurity.domain.entities.Role;
import com.example.alexandersecurity.domain.entities.User;
import com.example.alexandersecurity.domain.request.UserLoginRequest;
import com.example.alexandersecurity.domain.request.UserRegisterRequest;
import com.example.alexandersecurity.domain.response.RegisterResponse;
import com.example.alexandersecurity.exception.UsernamePasswordInvalidException;
import com.example.alexandersecurity.repositories.AuthorityRepository;
import com.example.alexandersecurity.repositories.RoleRepository;
import com.example.alexandersecurity.repositories.UserRepository;
import com.example.alexandersecurity.security.UserDetailsImpl;
import com.example.alexandersecurity.security.jwt.TokenProvider;
import com.example.alexandersecurity.service.AuthenticationService;
import com.example.alexandersecurity.service.CacheService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenProvider tokenProvider;
    private final AuthenticationManager authenticationManager;
    private final CacheService cacheService;

    @Override
    public RegisterResponse signUp(UserRegisterRequest request) {
        if (userRepository.existsByUsernameOrEmail(request.getUsername(), request.getEmail())) {
            throw new UsernamePasswordInvalidException(
                    String.format("User with %s or %s already exists", request.getUsername(), request.getEmail()));
        }

        Authority authority = authorityRepository.findAuthoritiesByAuthorityType(request.getAuthorityType());
        Role role = roleRepository.findRoleByRoleType(request.getRoleType());

        User user = new User();
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(Set.of(role));
        user.setAuthorities(Set.of(authority));
        user = userRepository.save(user);

        String token = tokenProvider.createToken(new UserDetailsImpl(user), null);
        String refreshToken = tokenProvider.refreshToken(new UserDetailsImpl(user), null);

        RegisterResponse registerResponse = new RegisterResponse();
        registerResponse.setToken(token);
        registerResponse.setRefreshToken(refreshToken);
        return registerResponse;

    }

    @Override
    public RegisterResponse signIn(UserLoginRequest request) {
        UserDetailsImpl userDetails = getUserFromRequest(request);
        if (Objects.nonNull(userDetails)) {
            String token = tokenProvider.createToken(userDetails, null);
            String refreshToken = tokenProvider.refreshToken(userDetails, null);
            RegisterResponse response = new RegisterResponse();
            response.setToken(token);
            response.setRefreshToken(refreshToken);
            return response;
        }
        return null;
    }

    private UserDetailsImpl getUserFromRequest(UserLoginRequest request) {
        try {
            String username = request.getUsername();
            Authentication authentication = null;
            if (StringUtils.hasText(username)) {
                try {
                    authentication = authenticationManager.authenticate(
                            new UsernamePasswordAuthenticationToken(username, request.getPassword()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (Objects.nonNull(authentication)) {
                return (UserDetailsImpl) authentication.getPrincipal();
            }
            return (UserDetailsImpl) authentication.getPrincipal();
        } catch (Exception e) {
            throw new UsernamePasswordInvalidException("Something went wrong");
        }
    }
}
