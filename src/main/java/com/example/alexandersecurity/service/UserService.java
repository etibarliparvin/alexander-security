package com.example.alexandersecurity.service;

import com.example.alexandersecurity.domain.entities.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findUserByEmail(String email);

    Optional<User> findUserByUsername(String username);
}
