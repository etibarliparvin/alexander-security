package com.example.alexandersecurity.domain.request;

import com.example.alexandersecurity.domain.enumerations.AuthorityType;
import com.example.alexandersecurity.domain.enumerations.RoleType;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserRegisterRequest {

    @NotBlank
    private String username;
    @NotBlank
    private String email;
    @NotBlank
    private String password;
    private RoleType roleType;
    private AuthorityType authorityType;
}
