package com.example.alexandersecurity.domain.response;

import lombok.Data;

@Data
public class RegisterResponse {

    private String token;
    private String refreshToken;
}
