package com.example.alexandersecurity.domain.enumerations;

public enum RoleType {
    ROLE_USER,
    ROLE_STUDENT,
    ROLE_AUTHOR
}
