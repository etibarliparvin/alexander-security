package com.example.alexandersecurity.domain.enumerations;

public enum UserStatus {
    ACTIVE,
    BLOCKED,
    FROZEN,
    INACTIVE,
    DELETED
}
