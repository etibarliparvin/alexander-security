package com.example.alexandersecurity.domain.enumerations;

public enum AuthorityType {
    CREATE,
    READ,
    UPDATE,
    DELETE
}
