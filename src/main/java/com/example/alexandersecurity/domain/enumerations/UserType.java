package com.example.alexandersecurity.domain.enumerations;

public enum UserType {
    USER,
    STUDENT,
    AUTHOR
}
