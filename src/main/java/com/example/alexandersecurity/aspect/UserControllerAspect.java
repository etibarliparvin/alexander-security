package com.example.alexandersecurity.aspect;

import com.example.alexandersecurity.domain.request.UserLoginRequest;
import com.example.alexandersecurity.service.CacheService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class UserControllerAspect {

    private final CacheService cacheService;

//    @Before("execution(* com.example.alexandersecurity.controllers.AuthController.signIn(..)) && args(request,..)")
//    public void loggingAspectBefore(UserLoginRequest request) {
//        log.info("Aspect -> method login is working : {}", request);
//    }
//
//    @After("execution(* com.example.alexandersecurity.controllers.AuthController.signIn(..))")
//    public void loggingAspectAfter(JoinPoint joinPoint) {
//        log.info("Aspect -> method login is done");
//    }

//    @Around("execution(* com.example.alexandersecurity.service.impl.AuthenticationServiceImpl.signIn(..)) && args(request,..)")
//    public Object foo(ProceedingJoinPoint joinPoint, UserLoginRequest request) throws Throwable {
//        log.info("Aspect -> method login is working : {}", request);
//        Object proceed = null;
//        try {
//            proceed = joinPoint.proceed();
//        } catch (UsernamePasswordInvalidException e) {
//            return ResponseEntity.status(400).body(ErrorResponseDto.builder()
//                    .status(400)
//                    .code(USERNAME_PASSWORD_INVALID_EXCEPTION.name())
//                    .message("!!! Bad Request !!!")
//                    .detail("!!! username or password is incorrect !!!")
//                    .timestamp(OffsetDateTime.now())
//                    .path(null)
//                    .build());
//        }
//        log.info("Aspect -> proceed: {}", proceed);
//        log.info("Aspect -> method login is done");
//        return proceed;
//    }

//    @Around("execution(* com.example.alexandersecurity.controllers.AuthController.signIn(..)) && args(request,..)")
//    public Object foo(ProceedingJoinPoint joinPoint, UserLoginRequest request) throws Throwable {
//        log.info("Aspect -> method login is working : {}", request);
//        Object proceed = null;
//        try {
//            proceed = joinPoint.proceed();
//        } catch (UsernamePasswordInvalidException e) {
//            return ResponseEntity.status(400).body(ErrorResponseDto.builder()
//                    .status(400)
//                    .code(USERNAME_PASSWORD_INVALID_EXCEPTION.name())
//                    .message("!!! Bad Request !!!")
//                    .detail("!!! username or password is incorrect !!!")
//                    .timestamp(OffsetDateTime.now())
//                    .path(null)
//                    .build());
//        }
//        log.info("Aspect -> proceed: {}", proceed);
//        log.info("Aspect -> method login is done");
//        return proceed;
//    }

    @Around("execution(* com.example.alexandersecurity.controllers.AuthController.signIn(..)) && args(request,..)")
    public Object foo(ProceedingJoinPoint joinPoint, UserLoginRequest request) throws Throwable {
        log.info("Aspect -> method login is working : {}", request);
        Object proceed = joinPoint.proceed();
        if (request.getPassword() == null) {
            String password = (String) cacheService.get(request.getUsername());
            if(password != null) {
                request.setPassword(password);
            }
        }
        log.info("Aspect -> proceed: {}", proceed);
        log.info("Aspect -> method login is done");
        return proceed;
    }
}
