package com.example.alexandersecurity.exception;

import lombok.Builder;
import lombok.Data;

import java.time.OffsetDateTime;
import java.util.Map;

@Data
@Builder
public class ErrorResponseDto {

    private int status;
    private String code;
    private String message;
    private String detail;
    private OffsetDateTime timestamp;
    private String path;
    private Map<String, Object> data;
}
