package com.example.alexandersecurity.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.OffsetDateTime;

import static com.example.alexandersecurity.exception.ErrorCode.USERNAME_PASSWORD_INVALID_EXCEPTION;
import static com.example.alexandersecurity.exception.ErrorCode.VALIDATION_EXCEPTION;

@RestControllerAdvice
public class UserManagementExceptionHandler {

//    @ExceptionHandler(UsernamePasswordInvalidException.class)
    public ResponseEntity<ErrorResponseDto> handleInvalidUsernameOrPassword(UsernamePasswordInvalidException ex,
                                                                            WebRequest webRequest) {
        String path = ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                        .status(400)
                        .code(USERNAME_PASSWORD_INVALID_EXCEPTION.name())
                        .message("!!! Bad Request !!!")
                        .detail("!!! username or password is incorrect !!!")
                        .timestamp(OffsetDateTime.now())
                        .path(path)
                .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto> methodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                            WebRequest webRequest) {
        String path = ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .code(VALIDATION_EXCEPTION.name())
                .message("Validation error")
                .detail(ex.getBindingResult().getFieldError().getField() + " " + ex.getBindingResult().getFieldError().getDefaultMessage())
                .timestamp(OffsetDateTime.now())
                .path(path)
                .build());
    }
}
