package com.example.alexandersecurity.exception;

public class UsernamePasswordInvalidException extends RuntimeException{


    public UsernamePasswordInvalidException(String message) {
        super(message);
    }
}
