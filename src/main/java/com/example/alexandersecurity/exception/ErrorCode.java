package com.example.alexandersecurity.exception;

public enum ErrorCode {
    USERNAME_PASSWORD_INVALID_EXCEPTION,
    VALIDATION_EXCEPTION
}
