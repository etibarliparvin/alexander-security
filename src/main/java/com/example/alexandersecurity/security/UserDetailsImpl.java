package com.example.alexandersecurity.security;

import com.example.alexandersecurity.domain.entities.Authority;
import com.example.alexandersecurity.domain.entities.Role;
import com.example.alexandersecurity.domain.entities.User;
import com.example.alexandersecurity.domain.enumerations.UserStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class UserDetailsImpl implements UserDetails {

    private final User user;

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        Set<Role> roles = user.getRoles();
        for (Role role : roles) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleType().name()));
        }
        Set<Authority> authorities = user.getAuthorities();
        for(Authority authority : authorities) {
            grantedAuthorities.add(new SimpleGrantedAuthority(authority.getAuthorityType().name()));
        }
        return grantedAuthorities;
//        return user.getRoles().stream()
//                .flatMap(role -> Stream.concat(
//                        // Stream for role itself (converted to SimpleGrantedAuthority)
//                        Stream.of(new SimpleGrantedAuthority(role.getRoleType().name())),
//                        // Stream for authorities within the role (converted to SimpleGrantedAuthority)
//                        role.getAuthorities().stream()
//                                .map(authority -> new SimpleGrantedAuthority(authority.getAuthorityType().name()))
//                )).collect(Collectors.toList()); // Collect the stream into a list
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getUserStatus() == UserStatus.ACTIVE;
    }
}
