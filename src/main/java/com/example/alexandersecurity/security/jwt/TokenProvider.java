package com.example.alexandersecurity.security.jwt;

import com.example.alexandersecurity.domain.entities.User;
import com.example.alexandersecurity.security.UserDetailsImpl;
import com.example.alexandersecurity.service.UserService;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class TokenProvider {

    @Autowired
    private UserService userService;
    private String secret =
            "ZWx4YW5wYXJ2aW5fcGFydmluZWx4YW5fZWx4YW5wYXJ2aW5fcGFydmluZWx4YW5fZWx4YW5lbHhhbl9wYXJ2aW5wYXJ2aW4=";
    private long jwtExpirationInMillis = 3600000; // one hour
    private long jwtRefreshExpirationInMillis = 5400000; // one and half hour
    private static final String AUTH_HEADER = "Authorization";
    private static final String BEARER = "Bearer ";

    private static final String AUTHORITIES_KEY = "auth";
    private final Key key;

    public TokenProvider(UserService userService) {
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        this.key = Keys.hmacShaKeyFor(keyBytes);
    }

    // when user gives username and password, system generates token for it
    public JwtBuilder createToken(UserDetails userDetails) {
        String authorities = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));

        JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + jwtExpirationInMillis))
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(key, SignatureAlgorithm.HS512);
        return jwtBuilder;
    }

    // when user gives username and password, system generates token for it
    public String createToken(UserDetails userDetails, Map<String, Object> extraClaims) {
        JwtBuilder jwtBuilder = createToken(userDetails);
        if (extraClaims == null) return jwtBuilder.compact();
        return jwtBuilder.setClaims(extraClaims).compact();
    }

    public String extractUsername(String token) {
        return extractClaims(token, Claims::getSubject);
    }

    public List<String> getAuthorities(String token) {
        return extractAllClaims(token).get("auth", List.class);
    }

    public <T> T extractClaims(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public Claims extractAllClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String refreshToken(UserDetails userDetails, Map<String, Object> extraClaims) {
        JwtBuilder jwtBuilder = createToken(userDetails);
        if (extraClaims == null)
            return jwtBuilder
                    .setExpiration(new Date(System.currentTimeMillis() + jwtRefreshExpirationInMillis))
                    .compact();
        return jwtBuilder
                .setClaims(extraClaims)
                .setExpiration(new Date(System.currentTimeMillis() + jwtRefreshExpirationInMillis))
                .compact();
    }

    public String refreshToken(String token, long duration) {
        if (isTokenExpired(token)) return "";
        Claims claims = extractAllClaims(token);
        Optional<User> user = userService.findUserByEmail(claims.getSubject());
        JwtBuilder refreshToken = createToken(new UserDetailsImpl(user.get()));
        return refreshToken.setExpiration(new Date(System.currentTimeMillis() + duration))
                .compact();
    }

    public boolean validateJwtToken(String token) {
        try {
            Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parse(token);
            return true;
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage(), e);
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage(), e);
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage(), e);
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage(), e);
        }
        return false;
    }

    public boolean validateJwtToken(String token, UserDetails userDetails) {
        final String email = extractUsername(token);
        return (email.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    public String removeBearerHeader(String header) {
        if (header.startsWith(BEARER)) {
            return header.substring(7);
        }
        throw new RuntimeException("This is not Bearer token");
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaims(token, Claims::getExpiration);
    }
}
