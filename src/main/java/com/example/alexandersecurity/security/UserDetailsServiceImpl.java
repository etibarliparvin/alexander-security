package com.example.alexandersecurity.security;

import com.example.alexandersecurity.service.UserService;
import com.example.alexandersecurity.utils.EmailValidationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;
    private final EmailValidationUtil emailValidationUtil;

    @Override
    public UserDetails loadUserByUsername(String key) throws UsernameNotFoundException {
        if (emailValidationUtil.patternMatches(key)) {
            return userService.findUserByEmail(key)
                    .map(UserDetailsImpl::new)
                    .orElseThrow(() -> new UsernameNotFoundException(String.format("User with %s not found", key)));
        }
        return userService.findUserByUsername(key)
                .map(UserDetailsImpl::new)
                .orElseThrow(() -> new UsernameNotFoundException(
                        String.format("User with %s not found", key)));
    }
}
